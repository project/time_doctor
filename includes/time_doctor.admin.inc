<?php

/**
 * @file
 * Settings for time doctor.
 */

/**
 * Menu callback: Settings.
 *
 * Configuration form to set configuration values.
 */
function time_doctor_settings_form($form, &$form_state) {

  $form['time_doctor_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Id'),
    '#default_value' => TIME_DOCTOR_CLIENT_ID,
    '#description' => t('Client Id for Time Doctor.'),
    '#required' => TRUE,
  );
  $form['time_doctor_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#default_value' => TIME_DOCTOR_SECRET_KEY,
    '#description' => t('Secret Key for Time Doctor.'),
    '#required' => TRUE,
  );
  $form['time_doctor_redirect_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect URL'),
    '#default_value' => time_doctor_get_base_url() . '/timedoctor/get-token',
    '#description' => t('Configure this URL in timedoctor app as Redirect url.'),
    '#attributes' => array('readonly' => 'readonly'),
  );

  $form = system_settings_form($form);
  // Custom submit handler for system settings form.
  $form['#submit'][] = 'time_doctor_create_access_token_submit';

  return $form;
}

/**
 * Implements hook_validate().
 */
function time_doctor_settings_form_validate($form, &$form_state) {
  $secret_key = $form_state['values']['time_doctor_secret_key'];
  if (preg_match('/^(\d+[a-z]|[a-z]+\d)[a-z\d]*$/i', $secret_key) != TRUE) {
    form_set_error('title', 'Secret key allows only alphanumeric values');
  }
}

/**
 * Submit handler for create access token.
 */
function time_doctor_create_access_token_submit($form, &$form_state) {
  $time_doctor_url = variable_get('time_doctor_url', NULL);
  $time_doctor_client_id = variable_get('time_doctor_client_id', NULL);
  $time_doctor_redirect_url = variable_get('time_doctor_redirect_url', NULL);
  $time_doctor_secret_key = variable_get('time_doctor_secret_key', NULL);
  drupal_goto($time_doctor_url . '/oauth/v2/auth?client_id=' . $time_doctor_client_id . ' &response_type=code&redirect_uri=' . $time_doctor_redirect_url);
}

/**
 * Callback from generate access token to set access token and refresh token.
 */
function time_doctor_get_access_token() {
  global $base_url;
  $auth_code = $_GET['code'];
  $data = time_doctor_trigger_request('get_access_token', '', '', $auth_code);
  $access_token = $data['access_token'];
  $refresh_token = $data['refresh_token'];
  variable_set('time_doctor_access_token', $access_token);
  variable_set('time_doctor_refresh_token', $refresh_token);
  drupal_set_message('Successfully set the access token');
  drupal_goto($base_url . '/admin/config/development/timedoctor/access_token');
}
